module.exports = function qotdUnschedule(args, {channel, guild}) {
  if (!this.bot.qotd) throw "disabled";

  if (this.bot.qotd.unschedule(guild.id)) {
    channel.send("Success");
  } else {
    channel.send("This server doesn't have QOTD setup.");
  }
  return true;
};

module.exports.description = "Cancel automatic QOTD";
module.exports.admin = true;
