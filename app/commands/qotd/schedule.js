module.exports = function qotdSchedule(args, {channel, guild}) {
  if (!this.bot.qotd) throw "disabled";

  if (args.length != 2 || isNaN(args[1])) throw "syntax";

  if (this.bot.qotd.schedule(guild.id, +args[1])) {
    channel.send("Success");
  } else {
    channel.send("This server doesn't have QOTD setup.");
  }
  return true;
};

module.exports.args = "<hour>";
module.exports.description = "Schedule automatic QOTD (uses bot's server time, check `&time`)";
module.exports.admin = true;
