module.exports = async function qotdSetup(args, {channel, mentions, guild}) {
  if (!this.bot.qotd) throw "disabled";

  const targetChannel = mentions.channels.first();
  const ping = mentions.roles.firstKey();
  if (!targetChannel) throw "syntax";
  if (!targetChannel.isText()) throw "target_channel_text";

  if (this.bot.qotd.setup(guild.id, targetChannel.id, ping)) {
    channel.send("Success");
  } else {
    channel.send("This server already has QOTD setup.");
  }
  return true;
};

module.exports.args = "<#channel> [@role to ping]";
module.exports.description = "Setup QOTD for this server";
module.exports.admin = true;
