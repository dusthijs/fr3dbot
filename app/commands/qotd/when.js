module.exports = function qotdWhen(args, {channel, guild}) {
  if (!this.bot.qotd) throw "disabled";

  if (guild.id in this.bot.qotd.jobs) {
    channel.send("Next QOTD will happen on "+this.bot.qotd.jobs[guild.id].nextInvocation());
  } else {
    channel.send("This server doesn't have automatic QOTD.");
  }
  return true;
};

module.exports.description = "Replies when will be the next automatic QOTD";
