module.exports = function parseQuestion(args, bot) {
  let _args = args.slice(1).join(" ").split(":");
  const question = _args.shift();
  const answers = [];

  _args = (_args[0] || "").split(" ");
  let lastEmoji, lastAnswer = [];

  for (let i = 0; i <= _args.length; i++) {
    const item = _args[i];
    const emoji = bot.resolveEmoji(item);

    if (emoji || !item) {
      if (lastEmoji && lastAnswer.length) {
        answers.push([lastEmoji, lastAnswer.join(" ")]);
        lastAnswer.length = 0;
      }

      lastEmoji = emoji;
    } else {
      lastAnswer.push(item);
    }
  }

  return [question, answers];
};
