module.exports = function ping(_, msg) {
  msg.channel.send({ content: "Pong!" });
  return true;
}

module.exports.description = "Pong!";
