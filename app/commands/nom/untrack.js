module.exports = async function nomUntrack(args, {channel}) {
  if (!this.bot.nom) throw "disabled";

  if (args.length < 2) throw "syntax";


  if (this.bot.nom.untrack(args[1])) {
    channel.send("Success");
    return true;
  } else {
    channel.send("Failed to untrack");
    return false;
  }
}

module.exports.args = "<role/user ID to stop watch for>";
module.exports.description = "Cancel tracking";
module.exports.admin = true;
