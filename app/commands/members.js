module.exports = async function members(args, {guild, channel}) {
  channel.send(`${guild.memberCount} members`);
  return true;
}

module.exports.description = "Show number of members";
