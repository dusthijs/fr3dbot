module.exports = async function pubsubUnsubscribe(args, {channel, mentions}) {
  if (!this.bot.pubsub) throw "disabled";

  const targetChannelID = mentions.channels.firstKey();

  if (args.length < 3 || !targetChannelID) throw "syntax";

  if (await this.bot.pubsub.unsubscribe(args[1], args[2], targetChannelID)) {
    channel.send("Success");
    return true;
  } else {
    channel.send("Wasn't subscribed");
    return false;
  }
}

module.exports.args = "<type> <id/name> <#channel>";
module.exports.description = "Cancel the notificiations for given profile in given channel.";
module.exports.admin = true;
