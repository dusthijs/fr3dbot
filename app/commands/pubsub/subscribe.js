module.exports = async function pubsubSubscribe(args, {channel, mentions}) {
  if (!this.bot.pubsub) throw "disabled";

  const targetChannel = mentions.channels.first();
  const targetRole = mentions.roles.firstKey();

  if (args.length < 3 || !targetChannel) throw "syntax";
  if (!targetChannel.isText()) throw "target_channel_text";


  if (await this.bot.pubsub.subscribe(args[1], args[2], targetChannel.id, targetRole)) {
    channel.send("Success");
    return true;
  } else {
    channel.send("Failed to subscribe");
    return false;
  }

}

module.exports.args = "<type> <id/name> <#channel> [@role to ping]";
module.exports.description = "Send notificiations to the specified channel optionally pinging the specified role.";
module.exports.admin = true;
