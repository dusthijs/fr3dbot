const table = require("text-table");

module.exports = async function pubsubList(args, {channel, guild, mentions}) {
  if (!this.bot.pubsub) throw "disabled";

  const type = args[1];
  const targetChannel = mentions.channels.first();

  const output = [];
  for (const [_type, adapter] of Object.entries(type ? {[type]: this.bot.pubsub.adapters[type]} : this.bot.pubsub.adapters)) {
    for (const [profileID, subscription] of Object.entries(adapter.subscriptions)) {
      for (const [channelID, {mention}] of Object.entries(subscription.channels)) {
        const notifyChannel = guild.channels.resolve(channelID);
        const notifyRole = guild.roles.resolve(mention);
        if (targetChannel ? targetChannel.id == channelID : notifyChannel) {
          output.push([
            ...(!type ? [_type] : []),
            profileID+("getName" in adapter ? ` (${await adapter.getName(profileID)})` : ""),
            "#"+notifyChannel.name,
            notifyRole ? "@"+notifyRole.name : "None"
          ]);
        }
      }
    }
  }

  const target = targetChannel ? targetChannel.toString() : "this server";
  channel.send(output.length ?
    (type || "All")+" notification subscriptions in "+target+":\n"+
    "```\n"+
    table([
      [...(!type ? ["Type"] : []), "Source", "Channel", "Role to ping"],
      ...output,
    ])+
    "\n```"
    :
    "There are no subscriptions yet in "+target
  );

  return true;
}

module.exports.args = "[type] [#channel]";
module.exports.description = "List notificiation subscriptions on this server or specified channel.";
module.exports.admin = true;
