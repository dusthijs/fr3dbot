const { MessageEmbed } = require("discord.js");
const { prefix } = global.config;

module.exports = function help(args, {channel, member}) {
  const help = [];
  const isAdmin = this.isAdmin(member);
  const {syntax, cmd, lastObject: commands} = this.resolveCommand(args.slice(1).join(" "));

  if (args.length > 1 && !cmd && !syntax.endsWith(" ")) {
    channel.send("Unknown command");
    return false;
  }

  if (typeof cmd !== "function") {
    for (const [name, command] of Object.entries(commands)) {
      if (name == "index") continue;
      let cmd = command;
      if (typeof command !== "function") {
        cmd = {
          ...command.index,
          args: "<subcommand>",
        };
      }
      if (!cmd.admin || isAdmin) {
        help.push([
          [syntax.substr(0,syntax.length-1), name].filter(v=>v).join(" "),
          cmd.args,
          cmd.description,
          cmd.admin
        ]);
      }
    }
  }

  const listItemsName = syntax ? "subcommand" : "command";
  const description = ((commands.index && commands.index.description) || (cmd && cmd.description));
  const embed = new MessageEmbed()
    .setTitle(
      (help.length ? listItemsName[0].toUpperCase()+listItemsName.slice(1)+"s" : "Help")+
      (syntax ? ` for command ${syntax}` : "")
    )
    .setDescription(
      (description ? `*${description}*\n\n` : "")+
      (help.length ?
        "Legend: `[]` means optional argument; `<>` means obligatory argument"+
        (isAdmin ? "; 🔒 means admin-only command" : "")+".\n"+
        `Use \`${args[0]} <${listItemsName}>\` for more info on each ${listItemsName}.\n`+
        "\n"+
        "**Commands:**\n"
      : "")
    )
    .addFields(help.map(([name, args, desc, admin]) => ({
      name: `\`${prefix}${name}${args ? ` ${args}` : ""}\`${admin ? " 🔒" : ""}`,
      value: desc,
    })));
  channel.send(embed);
  return true;
};

module.exports.args = "[command]";
module.exports.description = "Sends list of available commands";
