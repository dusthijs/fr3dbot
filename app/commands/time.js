module.exports = function time(_, {channel}) {
  channel.send(
    `My time is: ${new Date()}\n`+
    `Uptime: ${this.client.uptime}ms`
  );
};

module.exports.description = "Get bot's server time";
