module.exports = async function addReactroles(args, {channel, guild}) {
  if (!this.bot.reactroles) throw "disabled";
  const roles = [];
  const _args = args.slice(2);
  let i = 1;

  while (_args.length > 1) {
    let [emoji, role] = _args.splice(0, 2);

    emoji = this.bot.resolveEmoji(emoji);
    if (!emoji) {
      channel.send(`Emoji ${i} is either not valid or I can't use it.`);
      return false;
    }

    role = this.bot.resolveRole(role, guild);
    if (!role) {
      channel.send(`Role ${i} is not valid.`);
      return false;
    }

    roles.push([emoji, role]);
    i++;
  }

  if (!roles.length) throw "syntax";

  if (await this.bot.reactroles.add(guild.id, args[1], roles)) {
    channel.send("Success.");
    return true;
  } else {
    channel.send("The ID doesn't exist.");
    return false;
  }
};

module.exports.args = "<id> <emoji> <@role> [<emoji> <@role> [<emoji> <@role>...]]";
module.exports.description = "Add a role which members can assign to themselves with specified emoji (you can specify multiple pairs of emoji + role).";
module.exports.admin = true;
