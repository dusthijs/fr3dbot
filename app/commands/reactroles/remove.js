module.exports = async function removeReactroles(args, {channel, guild}) {
  if (!this.bot.reactroles) throw "disabled";
  const emojiOrRole = this.bot.resolveEmoji(args[2]) || this.bot.resolveRole(args[2], guild);
  if (args.length < 3 || !emojiOrRole) throw "syntax";

  if (await this.bot.reactroles.remove(guild.id, args[1], emojiOrRole)) {
    channel.send("Success.");
    return true;
  } else {
    channel.send("The ID, role or emoji doesn't exist.");
    return false;
  }
};

module.exports.args = "<id> <emoji or @role>";
module.exports.description = "Remove the specified role from the message, so members can no longer assign it to them. (Won't remove role from users.)";
module.exports.admin = true;
