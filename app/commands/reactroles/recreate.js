module.exports = async function recreateReactroles(args, {channel, guild}) {
  if (!this.bot.reactroles) throw "disabled";
  if (!args[1]) throw "syntax";
  if (await this.bot.reactroles.recreateEmbed(guild.id, args[1])) {
    channel.send("Success.");
    return true;
  } else {
    channel.send("The ID doesn't exist.");
    return false;
  }
};

module.exports.args = "<id>";
module.exports.description = "Re-edit or resend a reactroles message. Useful if the original message was deleted or broke.";
module.exports.admin = true;
