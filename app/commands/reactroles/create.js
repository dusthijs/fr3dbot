module.exports = async function createReactroles(args, {mentions, channel, guild}) {
  if (!this.bot.reactroles) throw "disabled";
  const targetChannel = mentions.channels.first();
  if (!targetChannel) throw "syntax";
  if (!targetChannel.isText()) throw "target_channel_text";

  const color = args[2] || null;
  const title = args.slice(3).join(" ");

  if (await this.bot.reactroles.create(guild, targetChannel, color, title)) {
    channel.send("Success.");
    return true;
  } else {
    channel.send("Creating reactroles failed. WTF did you do?");
    return false;
  }
};

module.exports.args = "<#channel> [color [title...]]";
module.exports.description = "Create a message that members can react to in the specified channel.";
module.exports.admin = true;
