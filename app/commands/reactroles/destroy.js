module.exports = async function destroyReactroles(args, {channel, guild}) {
  if (!this.bot.reactroles) throw "disabled";
  if (!args[1]) throw "syntax";
  if (await this.bot.reactroles.destroy(guild.id, args[1])) {
    channel.send("Success.");
    return true;
  } else {
    channel.send("The ID doesn't exist.");
    return false;
  }
};

module.exports.args = "<id>";
module.exports.description = "Delete the reactroles message. (Doesn't remove roles from users)";
module.exports.admin = true;
