const cooldown = 3*1000; // 3s

let lastTrigger = 0;

module.exports = function effect69({content, channel}) {
  if (content.match(/\b69\b/) && Date.now() - lastTrigger > cooldown) {
    channel.send("Nice");
  }

  return false; // let commands be processed
};
