const got = require("got");

const cooldown = 14.4e+6; // 4 hours

module.exports = class TwitchPubSub {
  constructor(bot, PubSubClient) {
    this.nameCache = {};
    this.subscriptions = {};

    this.refresh_token = null;
    this.headers = this._getToken().then(token => ({
      "Authorization": "Bearer " + token,
      "Client-ID": process.env.TWITCH_ID,
    }));

    this.pubsub = new PubSubClient(
      "twitchNotify",
      "https://api.twitch.tv/helix/webhooks/hub",
      "https://api.twitch.tv/helix/streams?user_id={}",
      (id, {data: inData}) => {
        const data = inData[0];
        if (!data || data.type != "live") return;
        const subscription = this.subscriptions[id];

        if (subscription.cache) {
          if (subscription.cache.started_at == data.started_at) return; // Ignore stream edits
          if (new Date() - new Date(subscription.cache.started_at) < cooldown) { // Ignore stream restarts
            subscription.cache = data;
            return;
          }
        }
        subscription.cache = data;


        for (const [channelID, {mention}] of Object.entries(subscription.channels)) {
          const message = bot.escapeMarkdown(data.user_name)+" is now live!"+
            (mention ? " <@&"+mention+">" : "" )+
            " https://twitch.tv/"+data.user_login; //#6441a5

          bot.client.channels.fetch(channelID).then(channel =>
            channel.send(message, { allowedMentions: { roles: [mention] } })
          );
        }
      }
    );
  }

  async _getToken() {
    return got.post("https://id.twitch.tv/oauth2/token"+
      "?client_id="+process.env.TWITCH_ID+
      "&client_secret="+process.env.TWITCH_TOKEN+
      "&grant_type="+(this.refresh_token ? "refresh_token" : "client_credentials")+
      (this.refresh_token ? "&refresh_token="+this.refresh_token : ""),
      { responseType: "json" },
    ).then(({ body }) =>
      body.access_token
    ).catch(e => console.error("Failed to authenticate:", e));
  }

  async getID(name) {
    if (name in this.nameCache) return this.nameCache[name];

    return got.get("https://api.twitch.tv/helix/search/channels?query="+name, {
      headers: await this.headers,
      responseType: "json",
    }).then(({body: {data}}) => {
      if (!data.length) throw "not found";
      return this.nameCache[name] = data[0].id;
    });
  }

  async getName(id) {
    for (const [name, itemID] of Object.entries(this.nameCache)) {
      if (itemID == id) return name;
    }

    return got.get("https://api.twitch.tv/helix/users?id="+id, {
      headers: await this.headers,
      responseType: "json",
    }).then(({body: {data}}) =>
      data.length && (this.nameCache[data[0].login] = id)
    );
  }

  async subscribe(name, channelID, mention) {
    const id = await this.getID(name);
    if (!(id in this.subscriptions)) {
      this.subscriptions[id] = {
        promise: this.pubsub.subscribe(id, await this.headers),
        cache: null,
        channels: {},
      };
    } else {
      // Prevent duplicates
      if (channelID in this.subscriptions[id].channels) {
        return false;
      }
    }

    this.subscriptions[id].channels[channelID] = {
      mention
    };

    return this.subscriptions[id].promise
      .catch(({response}) => console.error(response.body) && false);
  }

  async unsubscribe(name, channelID) {
    const id = await this.getID(name);
    if (!(id in this.subscriptions)) return false;

    const subscription = this.subscriptions[id];
    delete subscription.channels[channelID];

    // If no subscribers left, cancel the subscription enitrely
    if (!Object.keys(subscription.channels).length) this.pubsub.unsubscribe(id, await this.headers);

    return true;
  }
};
