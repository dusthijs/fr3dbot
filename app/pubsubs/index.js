module.exports = {
  instagram: require("./InstagramWatcher"),
  twitch: require("./TwitchPubSub"),
  youtube: require("./YoutubePubSub"),
};
