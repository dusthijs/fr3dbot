require("dotenv").config();
global.config = require("../config");

if (!process.env.DISCORD_TOKEN) throw Error("Missing the environment variable DISCORD_TOKEN");
if (!process.env.TWITCH_ID) throw Error("Missing the environment variable TWITCH_ID");
if (!process.env.TWITCH_TOKEN) throw Error("Missing the environment variable TWITCH_TOKEN");

const bot = require("./fr3dbot");
const webhookListener = require("./webhookListener");

const killHandler = () => {
  console.log("Logging out...");
  bot.client.destroy();
  process.exit(1);
};

bot.client.once("ready", () => {
  console.log("Ready");
  process.on("SIGINT", killHandler);
  process.on("SIGTERM", killHandler);
});

bot.start().catch(err => console.error("Boot failed", err));

webhookListener.start();
