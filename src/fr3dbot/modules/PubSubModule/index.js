const PubSubClient = require("./PubSubClient");
const adapters = require(process.cwd()+"/app/pubsubs");

module.exports = class PubSubModule {
  constructor(bot) {
    this.bot = bot;
    this.adapters = {};

    for (const [type, Adapter] of Object.entries(adapters)) {
      this.adapters[type] = new Adapter(bot, PubSubClient, this);
    }
  }

  load() {
    const { storage } = this.bot;
    for (const [type, subscriptions] of Object.entries(storage.data.pubsub = storage.data.pubsub || {})) {
      if (!(type in this.adapters)) continue;
  
      for (const data of subscriptions) {
        this.adapters[type].subscribe(...data);
      }
    }
  }

  async subscribe(type, id, channelID, mention) {
    if (!(type in this.adapters)) return false;

    const result = await this.adapters[type].subscribe(id, channelID, mention);
    if (result) {
      const pubsubData = this.bot.storage.data.pubsub;
      (pubsubData[type] = pubsubData[type] || []).push([id, channelID, mention]);
      this.bot.storage.save();
    }

    return result;
  }

  async unsubscribe(type, id, channelID) {
    if (!(type in this.adapters)) return false;

    const pubsubData = this.bot.storage.data.pubsub;
    if (type in pubsubData) {
      const item = pubsubData[type].find(v=>v[1] == channelID);
      if (!item) return false;
      pubsubData[type].splice(pubsubData[type].indexOf(item), 1);
      this.bot.storage.save();
    }

    return this.adapters[type].unsubscribe(id, channelID);
  }
};
