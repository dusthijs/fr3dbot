const { Collection, MessageEmbed } = require("discord.js");
const Color = require("color");

const defaultItem = {
  id: null,
  channelID: null,
  messageID: null,

  title: null,
  color: null,
  description: null,
  message: "",
  showHelp: true,
  showID: true,
  showRoles: true,

  roles: null,
  roleDescriptions: null,
};
const editableFields = {
  title: String,
  color: Color,
  description: String,
  message: String,
  showHelp: Boolean,
  showID: Boolean,
  showRoles: Boolean,
};

module.exports = class ReactionRolesModule {
  constructor(bot) {
    this.bot = bot;
    this.client = bot.client;
    this.storage = bot.storage;
    this.cache = {};
  }

  async load(guildID, item) {
    const guild = await this.client.guilds.fetch(guildID);
    const cache = {
      ...item,
      roles: new Collection(),
    };
    for (const [emoji, role] of item.roles) cache.roles.set(
      this.bot.resolveEmoji(`<:unnamed:${emoji}>`),
      this.bot.resolveRole(`<@&${role}>`, guild),
    );
    this._loadItem(guild, cache);
  }

  _loadItem(guild, cache) {
    this.bot.reactionHandler.listenAdd(cache.messageID, (reaction, user) =>
      this._handleAddReaction(cache, guild, reaction, user)
    );
    this.bot.reactionHandler.listenRemove(cache.messageID, (reaction, user) =>
      this._handleRemoveReaction(cache, guild, reaction, user)
    );

    (this.cache[guild.id] = this.cache[guild.id] || []).push(Object.assign({}, defaultItem, cache));
  }

  save() {
    for (const [guildID, guildCache] of Object.entries(this.cache)) {
      const guildData = this.storage.getGuild(guildID);
      guildData.reactroles = [];
      for (const item of guildCache) {
        if (!item) continue;
        guildData.reactroles.push({
          ...item,
          roles: item.roles.map((role, emoji) => [emoji+"", role+""]),
        });
      }
    }

    this.storage.save();
  }


  getItem(guildID, id) {
    return this.cache[guildID] && this.cache[guildID][id];
  }


  async create(guild, channel, color, title) {
    color = Color(color).hex();

    const id = this.cache[guild.id] ? this.cache[guild.id].length : 0;

    const cache = Object.assign({}, defaultItem, {
      id,
      channelID: channel.id,
      title,
      color,

      roles: new Collection(),
      roleDescriptions: {},
    });
    const msgid = cache.messageID = await this._renderEmbed(cache);
    if (!msgid) return false;

    this._loadItem(guild, cache);
    return true;
  }


  async destroy(guildID, id) {
    const item = this.cache[guildID].splice(id, 1)[0];
    if (!item) return false;

    const channel = await this.client.channels.fetch(item.channelID);
    if (!channel) return false;
    let message;
    try {
      message = await channel.messages.fetch(item.messageID);
      if (message && message.deleted) message = null;
    } catch (err) {
      if (err.message != "Unknown Message") throw err;
    }

    if (message) await message.delete();

    this.save();
    return true;
  }


  async recreateEmbed(guildID, id) {
    const item = this.getItem(guildID, id);
    if (!item) return false;
    return this._renderEmbed(item);
  }


  async _renderEmbed(item) {
    // const item = this.getItem(guildID, id);
    // if (!item) return false;
    let message;
    const channel = await this.client.channels.fetch(item.channelID);
    if (!channel) return false;

    if (item.messageID) {
      try {
        message = await channel.messages.fetch(item.messageID);
        if (message && message.deleted) message = null;
      } catch (err) {
        if (err.message != "Unknown Message") throw err;
      }
    }

    const embed = new MessageEmbed()
      .setTitle(item.title || "Choose your role")
      .setColor(item.color)
      .setDescription(
        (item.description ? item.description+"\n" : "")+
        (item.showHelp ? "React with emoji to recieve the role assigned to it, remove the reaction to lose the role.\n" : "")+
        (item.showHelp || item.description ? "\n" : "")+
        (item.showRoles ? item.roles.map((role, emoji) =>
          `${emoji} - ${role}` + (role.id in item.roleDescriptions ? ` - ${item.roleDescriptions[role.id]}` : "")
        ).join("\n") : "")+
        ""
      )
      .setFooter(item.showID ? `ID: ${item.id}` : "");

    const messageData = [
      item.message,
      { embed, allowedMentions: { parse: ["everyone"] } }
    ];
    if (message) message.edit(...messageData);
    else message = await channel.send(...messageData);

    // remove obsolete reactions
    for (const reaction of message.reactions.cache.array()) {
      if (!item.roles.has(reaction.emoji) && !item.roles.has(reaction.emoji.name)) {
        reaction.remove();
      }
    }

    // add all reactions
    let reactionPromise = Promise.resolve();
    for (const [emoji] of item.roles) {
      reactionPromise = reactionPromise.then(() =>
        message.react(emoji.id || emoji)
      );
    }

    this.save(); // Save current item data, because this method is usually called after changes
    return message.id;
  }


  async add(guildID, id, roles) {
    const item = this.getItem(guildID, id);
    if (!item) return false;

    for (const role of roles) item.roles.set(...role);

    return await this._renderEmbed(item);
  }


  async remove(guildID, id, emojiOrRole) {
    const item = this.getItem(guildID, id);
    if (!item) return false;

    const result = item.roles.delete(emojiOrRole) || item.roles.delete(item.roles.findKey(role => role.id == emojiOrRole.id));
    if (!result) return result;

    return await this._renderEmbed(item);
  }


  async describe(guildID, id, role, description) {
    const item = this.getItem(guildID, id);
    if (!item) return false;

    if (description) item.roleDescriptions[role.id] = description;
    else delete item.roleDescriptions[role.id];

    return await this._renderEmbed(item);
  }


  async _handleReaction(item, guild, reaction, user, remove) {
    if (user.id == this.client.user.id) return;
    const emoji = this.client.emojis.resolve(reaction.emoji.id) || reaction.emoji;
    const role = item.roles.get(emoji) || item.roles.get(emoji.name);
    if (!role) return; // Unexpected emoji
    const member = await guild.members.fetch(user.id);

    if (remove) return member.roles.remove(role);
    return member.roles.add(role);
  }

  _handleAddReaction(item, guild, reaction, user) {
    return this._handleReaction(item, guild, reaction, user, false);
  }

  _handleRemoveReaction(item, guild, reaction, user) {
    return this._handleReaction(item, guild, reaction, user, true);
  }
};

module.exports.editableFields = editableFields;
