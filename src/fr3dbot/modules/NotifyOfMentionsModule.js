const cooldown = 15000; // 15s

module.exports = class NotifyOfMentionsModule {
  constructor(bot) {
    this.bot = bot;
    this.storage = bot.storage;

    this.trackedMentions = {};
    this.registered = false;
  }

  load() {
    if (!this.registered) this.bot.effectHandler.register(this.handle.bind(this));
    this.registered = true;

    for (const item of (this.storage.data.nom = this.storage.data.nom || [])) {
      this.track(...item, true);
    }
  }

  save() {
    this.storage.data.nom.length = 0;

    for (const [mention, { ping, trackedRole, message }] of Object.entries(this.trackedMentions)) {
      this.storage.data.nom.push([mention, ping, trackedRole, message]);
    }
    return this.storage.save();
  }

  track(mention, ping, trackedRole, message, loading=false) {
    if (mention in this.trackedMentions) return false;
    this.trackedMentions[mention] = {
      ping,
      trackedRole,
      message,
      timestamp: 0,
    };
    if (!loading) this.save();
    return true;
  }

  untrack(mention) {
    if (!(mention in this.trackedMentions)) return false;
    delete this.trackedMentions[mention];
    this.save();
    return true;
  }

  handle(message) {
    const words = message.content.split(/ +/);

    for (const word of words) {
      const mention = this.bot.resolveMention(word, message.guild, true);
      if (!mention || !(mention in this.trackedMentions)) continue;
      const item = this.trackedMentions[mention];

      if (message.member.roles.highest.id != item.trackedRole) continue;

      // cooldown
      const d = Date.now();
      if (d - item.timestamp > cooldown) {
        message.reply(item.message, { allowedMentions: { users: [item.ping] } });
        item.timestamp = d;
      }

      return true;
    }

    return false;
  }
};
