const fs = require("fs");

const { prefix } = global.config;

const commands = {};

const traverse = (folder, cmdObj) => {
  const content = fs.readdirSync("./app/commands"+folder, { withFileTypes: true });

  for (const file of content) {
    if (file.name.startsWith("_")) continue;
    if (file.isDirectory()) {
      traverse(folder+file.name+"/", cmdObj[file.name] = {});
      continue;
    }

    const cmd = file.name.split(".")[0];
    cmdObj[cmd] = require(process.cwd()+"/app/commands"+folder+file.name);
  }
};
traverse("/", commands);

module.exports = class CommandHandler {
  constructor(bot) {
    this.bot = bot;
    this.storage = bot.storage;
    this.client = bot.client;
  }

  isAdmin(member) {
    return member.hasPermission("ADMINISTRATOR");
  }

  resolveCommand(msg) {
    const args = msg.replace(new RegExp("^"+prefix), "").split(/ +/);
    const result = {
      args,
      cmd: null,
      syntax: [],
      lastObject: commands,
    };

    const search = () => {
      const subcmd = args.shift() || "";
      result.syntax.push(subcmd);

      result.cmd = result.lastObject[subcmd.toLowerCase()];

      if (typeof result.cmd === "function" || !result.cmd) return;
      result.lastObject = result.cmd;
      return search();
    }
    search();

    result.syntax = result.syntax.join(" ");
    return result;
  }

  /**
   * Handles the message
   * @returns {boolean} True if the command was processed, false if we should await changes to the command
   */
  async handle(message){
    if (!message.content.startsWith(prefix)) return false;

    const {args, cmd, syntax} = this.resolveCommand(message.content);

    if (!cmd) {
      // Command not found
      if (!syntax.trim()) return false;
      // No subcommand given
      if (syntax.endsWith(" ")) {
        message.channel.send("Subcommand required"); // TODO: show available subcommands
        return false;
      }
      // Subcommand not found
      message.channel.send("Unknown subcommand");
      return false;
    }
    // Admin-only commands
    if (cmd.admin && !this.isAdmin(message.member)) return true;

    try {
      args.unshift(prefix+syntax);
      return await cmd.bind(this)(args, message);
    } catch (err) {
      // Expected (custom) error
      if (typeof err === "string") {
        switch(err) {
          // Command syntax error
          case "syntax":
            message.channel.send(`Usage: \`${prefix}${syntax} ${cmd.args}\``);
            return false;
          // Command target channel must be text
          case "target_channel_text":
            message.channel.send("The target channel must be textual.");
            return false;
          // Module disabled error
          case "disabled":
            message.channel.send("This command is currently disabled.");
            break;
          // Unexpected expected error
          default:
            message.channel.send(`Error: ${err}`);
        }
      // Unexpected error
      } else {
        console.error("Failed to execute a command:", err);
        message.channel.send(`Failed to execute a command: ${err.name}: ${err.message}`);
      }
      return true;
    }
  }
};

module.exports.commands = commands;
