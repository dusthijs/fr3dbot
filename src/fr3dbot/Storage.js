const fs = require("fs");

module.exports = class Storage {
  constructor(path) {
    this.path = path;

    this.data = {
      guilds: {},
    };
    this.load();
  }

  getGuild(guildid, defObj={}) {
    if (defObj) return this.data.guilds[guildid] = this.data.guilds[guildid] || defObj;
    return this.data.guilds[guildid];
  }

  load() {
    if (fs.existsSync(this.path)) {
      this.data = JSON.parse(fs.readFileSync(this.path));
    }
  }

  save() {
    return fs.promises.writeFile(this.path, JSON.stringify(this.data));
  }
}
