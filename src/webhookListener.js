const express = require("express");
const bodyParser = require("body-parser");
require("body-parser-xml")(bodyParser);

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.xml({ xmlParseOptions: { explicitArray: false } }));
app.use(bodyParser.urlencoded({extended: true}));

module.exports = app;

module.exports.start = () => {
  const server = app.listen(process.env.HTTP_PORT || 0, "localhost", ()=>{
    const addr = server.address();
    const url = (typeof addr == "string"
      ? addr
      : `http://${addr.address}:${addr.port}/`
    );
    console.info("Listening on", url);
  });
};
